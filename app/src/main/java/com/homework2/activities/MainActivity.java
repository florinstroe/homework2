package com.homework2.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.homework2.R;
import com.homework2.fragments.FragmentAlbums;
import com.homework2.fragments.FragmentImages;
import com.homework2.fragments.FragmentUsers;
import com.homework2.interfaces.ActivityFragmentCommunication;
import com.homework2.models.Album;
import com.homework2.models.User;

public class MainActivity extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addUsersFragment();
    }

    public void addUsersFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        FragmentTransaction addTransaction = transaction.add(R.id.frame_activity, new FragmentUsers());
        addTransaction.commit();
    }

    public void addAlbumsFragment(User user) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = FragmentAlbums.class.getName();
        FragmentTransaction addTransaction = transaction.replace(R.id.frame_activity, new FragmentAlbums(user), tag);
        transaction.addToBackStack(tag);
        addTransaction.commit();
    }

    public void addImagesFragment(Album album) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = FragmentImages.class.getName();
        FragmentTransaction addTransaction = transaction.replace(R.id.frame_activity, new FragmentImages(album), tag);
        transaction.addToBackStack(tag);
        addTransaction.commit();
    }
}