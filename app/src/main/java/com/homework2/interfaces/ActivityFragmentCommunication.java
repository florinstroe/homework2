package com.homework2.interfaces;

import com.homework2.models.Album;
import com.homework2.models.User;

public interface ActivityFragmentCommunication {
    void addAlbumsFragment(User user);
    void addImagesFragment(Album album);
}
