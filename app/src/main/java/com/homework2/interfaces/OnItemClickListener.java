package com.homework2.interfaces;

import com.homework2.models.Album;
import com.homework2.models.User;

public interface OnItemClickListener {
    public void onUserClick(User user);

    public void onArrowClicked(User user);

    public void onAlbumClick(Album album);

    public void onPostClick(int userId);
}
