package com.homework2.models

public enum class ItemType(val id: Int) {
    USER(0),
    POST(1)
}